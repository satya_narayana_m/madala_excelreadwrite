/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package madala_excelreadwrite;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FontUnderline;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;
/**
 *
 * @author S521743
 */
public class Madala_ExcelReadWrite {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            FileInputStream file = new FileInputStream(new File("Madala_Input.xlsx"));
            XSSFWorkbook readWorkbook = new XSSFWorkbook(file);
            XSSFSheet sheet = readWorkbook.getSheetAt(0);

            XSSFWorkbook writeWorkbook = new XSSFWorkbook();
            XSSFSheet writeSheet = writeWorkbook.createSheet("Movies");

            MovieList movieList = new MovieList();

            String genre = "";
            double rating = 0.0;
            String movieName = "";
            String directorName = "";
            Date releaseDate = new Date();
            double budget = 0.0;
            
            //Parsing through the Excel and storing Data
            Iterator<Row> rowIterator = sheet.iterator();
            rowIterator.next();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    switch (cell.getCellType()) {
                        case Cell.CELL_TYPE_NUMERIC:
                            if (cell.getColumnIndex() == 4) {
                                releaseDate = cell.getDateCellValue();
                            } else if (cell.getColumnIndex() == 5) {
                                budget = cell.getNumericCellValue();
                            } else if (cell.getColumnIndex() == 6) {
                                rating = cell.getNumericCellValue();
                            }
                            break;
                        case Cell.CELL_TYPE_STRING:
                            if (cell.getColumnIndex() == 1) {
                                movieName = cell.getStringCellValue();
                            } else if (cell.getColumnIndex() == 2) {
                                genre = cell.getStringCellValue();
                            } else if (cell.getColumnIndex() == 3) {
                                directorName = cell.getStringCellValue();
                            }
                            break;
                    }
                }

                movieList.addMovie(new Movie(genre, rating, movieName, directorName, releaseDate, budget));
            }
            
            Collections.sort(movieList.getMovieList());
            //Testing purpose
//            int testCount = 0;
//            for (Movie eachMovie : movieList) {
//                System.out.println(testCount +"   "+eachMovie.getGenre() +" ::::: "+ eachMovie.getRating());
//                testCount++;
//            }
            //To create Name row and add author name in the cell
            XSSFCellStyle nameStyle = writeWorkbook.createCellStyle();
            nameStyle.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            nameStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            nameStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            nameStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            nameStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            nameStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            XSSFFont nameFont = writeWorkbook.createFont();
            nameFont.setColor(HSSFColor.BLACK.index);
            nameFont.setBold(true);
            nameStyle.setFont(nameFont);

            XSSFCellStyle nameCell = writeWorkbook.createCellStyle();
            nameCell.setFillForegroundColor(HSSFColor.LIGHT_TURQUOISE.index);
            nameCell.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            nameCell.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            nameCell.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            nameCell.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            nameCell.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            XSSFFont nameCellFont = writeWorkbook.createFont();
            nameCellFont.setUnderline(FontUnderline.SINGLE);
            nameCellFont.setItalic(true);
            nameCellFont.setColor(HSSFColor.BLACK.index);
            nameCell.setFont(nameCellFont);
            //CellRangeAddress(int firstRow, int lastRow, int firstCol, int lastCol)
            writeSheet.addMergedRegion(new CellRangeAddress(1, 1, 2, 3));
            
            Row nameRow = writeSheet.createRow(1);
            nameRow.createCell(1).setCellStyle(nameStyle);
            nameRow.getCell(1).setCellValue("Name");
            nameRow.createCell(2).setCellStyle(nameCell);
            nameRow.getCell(2).setCellValue("Madala, Satyanarayana");
            //--------------------------------------------------//
            
            //Header style
            XSSFCellStyle headerStyle = writeWorkbook.createCellStyle();
            headerStyle.setFillForegroundColor(HSSFColor.DARK_RED.index);
            headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            headerStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            headerStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            headerStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            headerStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            XSSFFont headerFont = writeWorkbook.createFont();
            headerFont.setColor(HSSFColor.WHITE.index);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            Row headerRow = writeSheet.createRow(3);
            headerRow.createCell(0).setCellValue("SNO");
            headerRow.getCell(0).setCellStyle(headerStyle);
            headerRow.createCell(1).setCellValue("Genre");
            headerRow.getCell(1).setCellStyle(headerStyle);
            headerRow.createCell(2).setCellValue("Rating");
            headerRow.getCell(2).setCellStyle(headerStyle);
            headerRow.createCell(3).setCellValue("Movie Name");
            headerRow.getCell(3).setCellStyle(headerStyle);
            headerRow.createCell(4).setCellValue("Director");
            headerRow.getCell(4).setCellStyle(headerStyle);
            headerRow.createCell(5).setCellValue("Release Date");
            headerRow.getCell(5).setCellStyle(headerStyle);
            headerRow.createCell(6).setCellValue("Budget");
            headerRow.getCell(6).setCellStyle(headerStyle);
            //------------------------------------------------//
            int sno = 1;
            int firstRow = 4;
            //Date Format style
            DataFormat format = writeWorkbook.createDataFormat();
            XSSFCellStyle dateCellOneStyle = writeWorkbook.createCellStyle();
            dateCellOneStyle.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
            dateCellOneStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            dateCellOneStyle.setDataFormat(format.getFormat("dd-mmm-yy"));
            dateCellOneStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            dateCellOneStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            dateCellOneStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            dateCellOneStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            
            XSSFCellStyle dateCellTwoStyle = writeWorkbook.createCellStyle();
            dateCellTwoStyle.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            dateCellTwoStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            dateCellTwoStyle.setDataFormat(format.getFormat("dd-mmm-yy"));
            dateCellTwoStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            dateCellTwoStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            dateCellTwoStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            dateCellTwoStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            
            ArrayList<String> listGenre = new ArrayList();
            String headerGenre = movieList.getMovieList().get(0).getGenre();
            listGenre.add(headerGenre);
            for (int j = 0; j < movieList.getMovieList().size(); j++) {
                if (listGenre.contains(movieList.getMovieList().get(j).getGenre())) {

                } else {
                    listGenre.add(movieList.getMovieList().get(j).getGenre());
                }
            }
            //----------------------------------------------------//
            
            //cell styles for table body rows
            XSSFCellStyle cellStyleOne = writeWorkbook.createCellStyle();
            cellStyleOne.setFillForegroundColor(HSSFColor.LIGHT_BLUE.index);
            cellStyleOne.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            XSSFFont cellStyleOneFont = writeWorkbook.createFont();
            cellStyleOneFont.setColor(HSSFColor.BLACK.index);
            cellStyleOne.setFont(cellStyleOneFont);
            cellStyleOne.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            cellStyleOne.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            cellStyleOne.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            cellStyleOne.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            
            XSSFCellStyle cellStyleTwo = writeWorkbook.createCellStyle();
            cellStyleTwo.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
            cellStyleTwo.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            XSSFFont cellStyleTwoFont = writeWorkbook.createFont();
            cellStyleTwoFont.setColor(HSSFColor.BLACK.index);
            cellStyleTwo.setFont(cellStyleTwoFont);
            cellStyleTwo.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            cellStyleTwo.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            cellStyleTwo.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            cellStyleTwo.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            //FOr applying same color for altering geners sets//
            for (Movie eachMovie : movieList.getMovieList()) {
                
                Row row2 = writeSheet.createRow(firstRow);
                    Cell snoCell = row2.createCell(0);
                    snoCell.setCellValue(sno);
                    snoCell.setCellStyle(cellStyleOne);
                    Cell genreCell = row2.createCell(1);
                    genreCell.setCellValue(eachMovie.getGenre());
                    Cell ratingCell = row2.createCell(2);
                    genreCell.setCellStyle(cellStyleOne);
                    ratingCell.setCellValue(eachMovie.getRating());
                    ratingCell.setCellStyle(cellStyleOne);
                    Cell movieCell = row2.createCell(3);
                    movieCell.setCellValue(eachMovie.getMovieName());
                    movieCell.setCellStyle(cellStyleOne);
                    Cell directorCell = row2.createCell(4);
                    directorCell.setCellValue(eachMovie.getDirectorName());
                    directorCell.setCellStyle(cellStyleOne);
                    Cell releaseCell = row2.createCell(5);
                    releaseCell.setCellStyle(cellStyleOne);
                    releaseCell.setCellValue(eachMovie.getReleaseDate());

                    releaseCell.setCellStyle(dateCellOneStyle);
                    Cell budgetCell = row2.createCell(6);
                    budgetCell.setCellValue(eachMovie.getBudget());
                    budgetCell.setCellStyle(cellStyleOne);
                //Logic to apply color for different
                if (listGenre.indexOf(eachMovie.getGenre()) % 2 == 0) {
                    snoCell.setCellValue(sno);
                    snoCell.setCellStyle(cellStyleOne);
                    
                    genreCell.setCellValue(eachMovie.getGenre());
                    
                    genreCell.setCellStyle(cellStyleOne);
                    ratingCell.setCellValue(eachMovie.getRating());
                    ratingCell.setCellStyle(cellStyleOne);
                    
                    movieCell.setCellValue(eachMovie.getMovieName());
                    movieCell.setCellStyle(cellStyleOne);
                    
                    directorCell.setCellValue(eachMovie.getDirectorName());
                    directorCell.setCellStyle(cellStyleOne);
                    
                    releaseCell.setCellStyle(cellStyleOne);
                    releaseCell.setCellValue(eachMovie.getReleaseDate());

                    releaseCell.setCellStyle(dateCellOneStyle);
                    
                    budgetCell.setCellValue(eachMovie.getBudget());
                    budgetCell.setCellStyle(cellStyleOne);
                    
                } else {
                   
                    snoCell.setCellValue(sno);
                    snoCell.setCellStyle(cellStyleTwo);
                    
                    genreCell.setCellValue(eachMovie.getGenre());
                 
                    genreCell.setCellStyle(cellStyleTwo);
                    ratingCell.setCellValue(eachMovie.getRating());
                    ratingCell.setCellStyle(cellStyleTwo);
                    
                    movieCell.setCellValue(eachMovie.getMovieName());
                    movieCell.setCellStyle(cellStyleTwo);
                    
                    directorCell.setCellValue(eachMovie.getDirectorName());
                    directorCell.setCellStyle(cellStyleTwo);
                    
                    releaseCell.setCellStyle(cellStyleTwo);
                    releaseCell.setCellValue(eachMovie.getReleaseDate());

                    releaseCell.setCellStyle(dateCellTwoStyle);
                  
                    budgetCell.setCellValue(eachMovie.getBudget());
                    budgetCell.setCellStyle(cellStyleTwo);
                    
                }
                    sno++;
                    firstRow++;
            }

            try (FileOutputStream outputStream = new FileOutputStream(new File("Madala_Output.xlsx"))) {
                writeWorkbook.write(outputStream);
                outputStream.close();
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }
        
        
        
    
    
}
