/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package madala_excelreadwrite;

import java.util.Date;

/**
 *
 * @author S521743
 */
public class Movie implements Comparable<Movie>{
    private String genre;
    private Double rating;
    private String movieName;
    private String directorName;
    private Date releaseDate;
    private Double budget;

    public Movie(String genre, Double rating, String movieName, String directorName, Date releaseDate, Double budget) {
        this.genre = genre;
        this.rating = rating;
        this.movieName = movieName;
        this.directorName = directorName;
        this.releaseDate = releaseDate;
        this.budget = budget;
    }

    public String getGenre() {
        return genre;
    }

    public Double getRating() {
        return rating;
    }

    public String getMovieName() {
        return movieName;
    }

    public String getDirectorName() {
        return directorName;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public Double getBudget() {
        return budget;
    }

    @Override
    public int compareTo(Movie o) {
    if (this.genre.compareTo(o.getGenre()) == 0){
        return o.getRating().compareTo(this.rating);
    }else{
        return this.genre.compareTo(o.getGenre());
    }
    }
    
    
    
}
