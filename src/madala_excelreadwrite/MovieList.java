/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package madala_excelreadwrite;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author S521743
 */
public class MovieList implements Iterable<Movie>{
    private ArrayList<Movie> movieList;

    public MovieList() {
        this.movieList = new ArrayList<>();
    }

    public ArrayList<Movie> getMovieList() {
        return movieList;
    }
    
    /**
     * Function to add movie to the list
     * @param movie 
     */
   public void addMovie(Movie movie){
        this.movieList.add(movie);
    }

    @Override
    public Iterator<Movie> iterator() {
        return this.movieList.iterator();
    }
}
